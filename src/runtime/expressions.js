var ls;
(function (ls) {
	ls.MainScene = function() {
		return {
			"%22newScene1%22": function() { return "newScene1" },
			"%22backIn%22": function() { return "backIn" },
			"%22tween2%22": function() { return "tween2" },
			"%22bounceInOut%22": function() { return "bounceInOut" },
			"%22tween%22": function() { return "tween" }
		}
	};
	ls.newScene1 = function() {
		return {
			"AISprite4642": function() { return AISprite4642 },
			"%22HP%22%2B%20AISprite570.x2": function() { return "HP"+ AISprite570.x2 },
			"%22hp%22": function() { return "hp" },
			"AISprite4582.HP%20-%202": function() { return AISprite4582.HP - 2 },
			"100%2Bls.random()*50": function() { return 100+ls.random()*50 },
			"%22newScene2%22": function() { return "newScene2" },
			"300%2Bls.random()*50": function() { return 300+ls.random()*50 },
			"AISprite4597": function() { return AISprite4597 },
			"%22lessThan%22": function() { return "lessThan" },
			"%22newScene3%22": function() { return "newScene3" },
			"AISprite4582.HP%20-%203": function() { return AISprite4582.HP - 3 },
			"%22fenshu%22": function() { return "fenshu" },
			"AISprite4522": function() { return AISprite4522 },
			"%22greaterOrEqual%22": function() { return "greaterOrEqual" },
			"%22greaterThan%22": function() { return "greaterThan" },
			"AISprite4633": function() { return AISprite4633 },
			"%22lessOrEqual%22": function() { return "lessOrEqual" },
			"AISprite579": function() { return AISprite579 },
			"AISprite4582": function() { return AISprite4582 },
			"AISprite570.fenshu": function() { return AISprite570.fenshu },
			"%22equalTo%22": function() { return "equalTo" },
			"AISprite4627": function() { return AISprite4627 },
			"%22movetag%22": function() { return "movetag" },
			"AISprite576": function() { return AISprite576 },
			"AISprite4639": function() { return AISprite4639 },
			"40%2Bls.random()*340": function() { return 40+ls.random()*340 },
			"%22x2%22": function() { return "x2" },
			"AISprite573": function() { return AISprite573 },
			"AISprite4624": function() { return AISprite4624 },
			"%22HP%22": function() { return "HP" },
			"%22HP%22%2BAISprite570.x2": function() { return "HP"+AISprite570.x2 },
			"AISprite4636": function() { return AISprite4636 },
			"AISprite570": function() { return AISprite570 },
			"AISprite4582.HP%20-%201": function() { return AISprite4582.HP - 1 }
		}
	};
	ls.newScene2 = function() {
		return {
			"%22newScene1%22": function() { return "newScene1" }
		}
	};
})(ls || (ls = {}));